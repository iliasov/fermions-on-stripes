# Fermions on stripes

The project calculates solution of Einstein equations with one non-diagonal component of metric and solves Dirac equations on that background.

Basically, project divides in two parts: generation of equations with boundary conditions (Einstein and Dirac) and a solving part.
Equations are generated with using of Wolfram Mathematica, numerical routine uses Python.

Folders:

dirac -- solving of Dirac equations

equations -- Wolfram Mathematica part

gravity -- solving of Einstein equations

math2py -- parsing files

output -- images output


At the moment, a technical documantation is being written.
If you have any question about the project, you are welcome to write to A.Iliasov@science.ru.nl
____________________________________________

1) Gravity side

Solving of gravity equations consists of 2 parts.
At first, equations are generated via Mathematica:

*/equations/Eineq_Stripes.nb* generates equations
*/equations/Eineq_Stripes_boundary_conditions.nb* generates boundary conditions

The result is some *.dat* files.

In the following step these equations are parsed into Python via
*/math2py/Math_Python_parser.py*

The result are *.txt* files.

The equations there are in numpy notation and then they are copied into file
*gravity/AdS_equations_norm_QS_trans.py*
This file contains all equations, boundary conditions and functions needed to calculate this equations.
The main function there is *AdS_stripes_norm()*. It returns values of these equations on a grid.
File */gravity/Jacobian_Newton_QS_trans.py* contains Newton routine.

The file for calculation is */gravity/Stripes_QS_trans.py*
Function *solution_func()* calculates solution for Einstein equations from the initial values of metric and scalar field.
the result is a *.txt* file.
Function *define_Fields()* reads results of solution for new initial values of metric and scalar field.

The file */output/metric_reading.py* contains function *show_result()*, which is used to see calculated metric and scalar field.