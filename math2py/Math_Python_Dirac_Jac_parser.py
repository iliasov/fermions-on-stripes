import re
import numpy as np
def write_func(read_name,write_name, type):


    file = open(read_name, "r")
    eq = file.read()
    file.close()

    # eq="Sqrt[2*x]+Sqrt[2+1]"
    # print(eq)

    eq=re.sub('\s',"",eq)
    "Replace of the first derivatives"
    eq=eq.replace("Derivative[0,1][Qtt][z,x]","DFields[..., 0, 1]")
    eq=eq.replace("Derivative[0,1][Qxx][z,x]","DFields[..., 1, 1]")
    eq=eq.replace("Derivative[0,1][Qyy][z,x]","DFields[..., 2, 1]")
    eq=eq.replace("Derivative[0,1][Qzz][z,x]","DFields[..., 3, 1]")
    eq=eq.replace("Derivative[0,1][Qxz][z,x]","DFields[..., 4, 1]")

    eq=eq.replace("Derivative[0,1][Qtt][0,x]","DFields[:,N-1, 0, 1]")
    eq=eq.replace("Derivative[0,1][Qxx][0,x]","DFields[:,N-1, 1, 1]")
    eq=eq.replace("Derivative[0,1][Qyy][0,x]","DFields[:,N-1, 2, 1]")
    eq=eq.replace("Derivative[0,1][Qzz][0,x]","DFields[:,N-1, 3, 1]")
    eq=eq.replace("Derivative[0,1][Qxz][0,x]","DFields[:,N-1, 4, 1]")

    eq=eq.replace("Derivative[0,1][\[Psi]1][x,z]","DSpinor[..., 0, 3]")
    eq=eq.replace("Derivative[0,1][\[Psi]2][x,z]","DSpinor[..., 1, 3]")
    eq=eq.replace("Derivative[0,1][\[Psi]3][x,z]","DSpinor[..., 2, 3]")
    eq=eq.replace("Derivative[0,1][\[Psi]4][x,z]","DSpinor[..., 3, 3]")

    eq=eq.replace("Derivative[1,0][Qtt][z,x]","DFields[..., 0, 3]")
    eq=eq.replace("Derivative[1,0][Qxx][z,x]","DFields[..., 1, 3]")
    eq=eq.replace("Derivative[1,0][Qyy][z,x]","DFields[..., 2, 3]")
    eq=eq.replace("Derivative[1,0][Qzz][z,x]","DFields[..., 3, 3]")
    eq=eq.replace("Derivative[1,0][Qxz][z,x]","DFields[..., 4, 3]")

    eq=eq.replace("Derivative[1,0][Qtt][0,x]","DFields[:,N-1, 0, 3]")
    eq=eq.replace("Derivative[1,0][Qxx][0,x]","DFields[:,N-1, 1, 3]")
    eq=eq.replace("Derivative[1,0][Qyy][0,x]","DFields[:,N-1, 2, 3]")
    eq=eq.replace("Derivative[1,0][Qzz][0,x]","DFields[:,N-1, 3, 3]")
    eq=eq.replace("Derivative[1,0][Qxz][0,x]","DFields[:,N-1, 4, 3]")

    eq=eq.replace("Derivative[1,0][\[Psi]1][x,z]","DSpinor[..., 0, 1]")
    eq=eq.replace("Derivative[1,0][\[Psi]2][x,z]","DSpinor[..., 1, 1]")
    eq=eq.replace("Derivative[1,0][\[Psi]3][x,z]","DSpinor[..., 2, 1]")
    eq=eq.replace("Derivative[1,0][\[Psi]4][x,z]","DSpinor[..., 3, 1]")

    eq=eq.replace("Derivative[1,0,0,0][\[Psi]1][x,z]","DSpinor[..., 0, 0]")
    eq=eq.replace("Derivative[1,0,0,0][\[Psi]2][x,z]","DSpinor[..., 1, 0]")
    eq=eq.replace("Derivative[1,0,0,0][\[Psi]3][x,z]","DSpinor[..., 2, 0]")
    eq=eq.replace("Derivative[1,0,0,0][\[Psi]4][x,z]","DSpinor[..., 3, 0]")

    eq=eq.replace("Derivative[0,0,1,0][\[Psi]1][x,z]","DSpinor[..., 0, 2]")
    eq=eq.replace("Derivative[0,0,1,0][\[Psi]2][x,z]","DSpinor[..., 1, 2]")
    eq=eq.replace("Derivative[0,0,1,0][\[Psi]3][x,z]","DSpinor[..., 2, 2]")
    eq=eq.replace("Derivative[0,0,1,0][\[Psi]4][x,z]","DSpinor[..., 3, 2]")


    "Replace of Fx's"

    eq=eq.replace("Qtt[z,x]","Fields[..., 0]")
    eq=eq.replace("Qxx[z,x]","Fields[..., 1]")
    eq=eq.replace("Qyy[z,x]","Fields[..., 2]")
    eq=eq.replace("Qzz[z,x]","Fields[..., 3]")
    eq=eq.replace("Qxz[z,x]","Fields[..., 4]")

    eq=eq.replace("Qtt[0,x]","Fields[:,N-1, 0]")
    eq=eq.replace("Qxx[0,x]","Fields[:,N-1, 1]")
    eq=eq.replace("Qyy[0,x]","Fields[:,N-1, 2]")
    eq=eq.replace("Qzz[0,x]","Fields[:,N-1, 3]")
    eq=eq.replace("Qxz[0,x]","Fields[:,N-1, 4]")


    eq=eq.replace("\[Psi]1[x,z]","Spinor[..., 0]")
    eq=eq.replace("\[Psi]2[x,z]","Spinor[..., 1]")
    eq=eq.replace("\[Psi]3[x,z]","Spinor[..., 2]")
    eq=eq.replace("\[Psi]4[x,z]","Spinor[..., 3]")

    eq=eq.replace("a0[x]","Spinor[:,N-1, 0]")
    eq=eq.replace("b0[x]","Spinor[:,N-1, 1]")
    eq=eq.replace("c0[x]","Spinor[:,N-1, 2]")
    eq=eq.replace("d0[x]","Spinor[:,N-1, 3]")

    eq=eq.replace("Derivative[1][a0][x]","DSpinor[:,N-1, 0, 1]")
    eq=eq.replace("Derivative[1][b0][x]","DSpinor[:,N-1, 1, 1]")
    eq=eq.replace("Derivative[1][c0][x]","DSpinor[:,N-1, 2, 1]")
    eq=eq.replace("Derivative[1][d0][x]","DSpinor[:,N-1, 3, 1]")

    eq=eq.replace("a1[x]","DSpinor[:,N-1, 0, 3]")
    eq=eq.replace("b1[x]","DSpinor[:,N-1, 1, 3]")
    eq=eq.replace("c1[x]","DSpinor[:,N-1, 2, 3]")
    eq=eq.replace("d1[x]","DSpinor[:,N-1, 3, 3]")

    eq=eq.replace("A0[x,z]","Psi")

    "Replace of other symbols"
    eq=eq.replace("^","**")
    eq=eq.replace("\[Mu]1","theta")
    eq=eq.replace("\[Omega]","omega")

    eq=eq.replace("Pi","np.pi")
    eq=eq.replace("Pi","np.pi")
    eq=eq.replace("P[z]","P_z")
    eq = eq.replace("P[0]", "P_0")
    eq=eq.replace("Derivative[1][P][z]","DP_z")
    eq = eq.replace("Derivative[1][P][0]", "DP_0")
    #eq=eq.replace("Derivative[2][P][z]","4*z**2*(2-3*theta**2*(1-z**2))-2*(1+2*(1-z**2)-1.5*theta**2*(1-z**2)**2)")
    eq=eq.replace("I","1j")

    "Replace of square root"

    while 1:
        sqin=eq.find("Sqrt[")
        if sqin==-1:
            break
        endtest=0
        sqout=0
        j=sqin+5
        for i in range(len(eq)-(sqin+5)):
            if eq[j]=="[":
                endtest+=1
            if eq[j] == "]":
                endtest -= 1
            if endtest==-1:
                sqout=j
                # print(eq[j], j, endtest)
                break
            # print(eq[j], j, endtest)
            j+=1
        eq=eq[:sqout]+")**(0.5)"+eq[(sqout+1):]
        eq=eq[:sqin]+"("+eq[(sqin+5):]
        # print(eq)
    print(eq)
    "Write on file"

    eq=re.sub('[\{\}]',"",eq)
    eq=re.sub(r'([\w|\)\]]),([-\w|\(\[])',r'\1\n\n\2', eq)
    eq = eq.replace("0,0\n", "0\n\n0\n")
    eq = eq.replace("0,0,0\n", "0\n\n0\n\n0\n")
    eq=re.sub(r'0,([a-zA-Z-(])',r'0\n\n\1',eq)
    eq=re.sub(r'0,([1-9])',r'0\n\n\1',eq)

    file = open(write_name, "w")
    file.write(eq)
    file.close()

    "TEST"
    N=7
    z=1.2*np.ones((7,7))
    Psi=np.ones((7,7))
    Spinor=0.9*np.ones((7,7,7))
    DSpinor=0.9*np.ones((7,7,7,7))
    Fields=0.9*np.ones((7,7,7))
    DFields=np.ones((7,7,7,7))
    k0=1
    K=1
    P_z=1
    DP_z=1
    P_0=1
    DP_0=1
    omega=1
    ky=1
    theta=1
    L=1
    m=1
    q=1
    file = open(write_name, "r")
    eq_test=file.read()
    file.close()
    y=eq_test.split("\n\n")

    if type=="Eq":
        for i in range(4):
            print(eval(y[i]))
        for i in range(len(y)):
            y[i]="result[...,"+str(i)+"]="+y[i]
        s='\n\n'
        file = open(write_name, "w")
        file.write(s.join(y))
        file.close()

    elif type=="Xcoef":
        str_write=[]
        for i in range(4):
            print(eval(y[i]))
        for i in range(len(y)):
            if y[i]!="0":
                str_write.append("Xcoef[...,"+str(i//4)+","+str(i%4)+"]="+y[i])
        s='\n\n'
        file = open(write_name, "w")
        file.write(s.join(str_write))
        file.close()

    elif type=="Zcoef":
        str_write=[]
        for i in range(4):
            print(eval(y[i]))
        for i in range(len(y)):
            if y[i]!="0":
                str_write.append("Zcoef[...,"+str(i//4)+","+str(i%4)+"]="+y[i])
        s='\n\n'
        file = open(write_name, "w")
        file.write(s.join(str_write))
        file.close()

    elif type=="Fcoef":
        str_write=[]
        for i in range(4):
            print(eval(y[i]))
        for i in range(len(y)):
            if y[i]!="0":
                str_write.append("Fcoef[...,"+str(i//4)+","+str(i%4)+"]="+y[i])
        s='\n\n'
        file = open(write_name, "w")
        file.write(s.join(str_write))
        file.close()

def collect(files_in,file_out):
    str_write=""
    s='\n'
    for i in range(len(files_in)):
        #print (i)
        file = open(files_in[i], "r")
        str_read=file.read()
        file.close()
        str_write=str_write+"\n\n"+str_read
    #print(str_write)
    file = open(file_out, "w")
    file.write(str_write)
    file.close()




read_name="Dirac_Stripes.dat"
write_name="Dirac_Stripes.txt"
type="Eq"
write_func(read_name,write_name, type)
#
read_nameX="Xcoef_Stripes.dat"
write_nameX="Xcoef_Stripes.txt"
type="Xcoef"
write_func(read_nameX,write_nameX, type)

read_nameZ="Zcoef_Stripes.dat"
write_nameZ="Zcoef_Stripes.txt"
type="Zcoef"
write_func(read_nameZ,write_nameZ, type)

read_nameF="Fcoef_Stripes.dat"
write_nameF="Fcoef_Stripes.txt"
type="Fcoef"
write_func(read_nameF,write_nameF, type)

files_in=[write_nameX,write_nameZ,write_nameF]
file_out="Coefficients_Stripes.txt"
collect(files_in,file_out)

read_name="Dirac_BC.dat"
write_name="Dirac_BC.txt"
type="Eq"
write_func(read_name,write_name, type)

# read_nameF="FcoefGR_Stripes.dat"
# write_nameF="FcoefGR_Stripes.txt"
# type="Fcoef"
# write_func(read_nameF,write_nameF, type)