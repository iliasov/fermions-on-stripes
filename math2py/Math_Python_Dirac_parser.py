import re
import numpy as np

read_name="Dirac_Stripes.dat"
write_name="Dirac_Stripes.txt"

file = open(read_name, "r")
eq = file.read()
file.close()

# eq="Sqrt[2*x]+Sqrt[2+1]"
# print(eq)

eq=re.sub('\s',"",eq)
"Replace of the first derivatives"
eq=eq.replace("Derivative[0,1][Qtt][z,x]","DFields[..., 0, 1]")
eq=eq.replace("Derivative[0,1][Qxx][z,x]","DFields[..., 1, 1]")
eq=eq.replace("Derivative[0,1][Qyy][z,x]","DFields[..., 2, 1]")
eq=eq.replace("Derivative[0,1][Qzz][z,x]","DFields[..., 3, 1]")
eq=eq.replace("Derivative[0,1][Qxz][z,x]","DFields[..., 4, 1]")

eq=eq.replace("Derivative[0,0,0,1][\[Psi]1][t,x,y,z]","DSpinor[..., 0, 3]")
eq=eq.replace("Derivative[0,0,0,1][\[Psi]2][t,x,y,z]","DSpinor[..., 1, 3]")
eq=eq.replace("Derivative[0,0,0,1][\[Psi]3][t,x,y,z]","DSpinor[..., 2, 3]")
eq=eq.replace("Derivative[0,0,0,1][\[Psi]4][t,x,y,z]","DSpinor[..., 3, 3]")

eq=eq.replace("Derivative[1,0][Qtt][z,x]","DFields[..., 0, 3]")
eq=eq.replace("Derivative[1,0][Qxx][z,x]","DFields[..., 1, 3]")
eq=eq.replace("Derivative[1,0][Qyy][z,x]","DFields[..., 2, 3]")
eq=eq.replace("Derivative[1,0][Qzz][z,x]","DFields[..., 3, 3]")
eq=eq.replace("Derivative[1,0][Qxz][z,x]","DFields[..., 4, 3]")

eq=eq.replace("Derivative[0,1,0,0][\[Psi]1][t,x,y,z]","DSpinor[..., 0, 1]")
eq=eq.replace("Derivative[0,1,0,0][\[Psi]2][t,x,y,z]","DSpinor[..., 1, 1]")
eq=eq.replace("Derivative[0,1,0,0][\[Psi]3][t,x,y,z]","DSpinor[..., 2, 1]")
eq=eq.replace("Derivative[0,1,0,0][\[Psi]4][t,x,y,z]","DSpinor[..., 3, 1]")

eq=eq.replace("Derivative[1,0,0,0][\[Psi]1][t,x,y,z]","DSpinor[..., 0, 0]")
eq=eq.replace("Derivative[1,0,0,0][\[Psi]2][t,x,y,z]","DSpinor[..., 1, 0]")
eq=eq.replace("Derivative[1,0,0,0][\[Psi]3][t,x,y,z]","DSpinor[..., 2, 0]")
eq=eq.replace("Derivative[1,0,0,0][\[Psi]4][t,x,y,z]","DSpinor[..., 3, 0]")

eq=eq.replace("Derivative[0,0,1,0][\[Psi]1][t,x,y,z]","DSpinor[..., 0, 2]")
eq=eq.replace("Derivative[0,0,1,0][\[Psi]2][t,x,y,z]","DSpinor[..., 1, 2]")
eq=eq.replace("Derivative[0,0,1,0][\[Psi]3][t,x,y,z]","DSpinor[..., 2, 2]")
eq=eq.replace("Derivative[0,0,1,0][\[Psi]4][t,x,y,z]","DSpinor[..., 3, 2]")


"Replace of Fx's"

eq=eq.replace("Qtt[z,x]","Fields[..., 0]")
eq=eq.replace("Qxx[z,x]","Fields[..., 1]")
eq=eq.replace("Qyy[z,x]","Fields[..., 2]")
eq=eq.replace("Qzz[z,x]","Fields[..., 3]")
eq=eq.replace("Qxz[z,x]","Fields[..., 4]")


eq=eq.replace("\[Psi]1[t,x,y,z]","Spinor[..., 0]")
eq=eq.replace("\[Psi]2[t,x,y,z]","Spinor[..., 1]")
eq=eq.replace("\[Psi]3[t,x,y,z]","Spinor[..., 2]")
eq=eq.replace("\[Psi]4[t,x,y,z]","Spinor[..., 3]")

eq=eq.replace("A0[x,z]","Psi")

"Replace of other symbols"
eq=eq.replace("^","**")
eq=eq.replace("\[Mu]1","theta")
eq=eq.replace("Pi","np.pi")
eq=eq.replace("P[z]","(1 + z + z ** 2 - 0.5 * theta**2 * z ** 3)")
eq=eq.replace("P[1]","(3 - 0.5 * theta**2)")
eq=eq.replace("Derivative[1][P][z]","(1+2*z-1.5*theta**2*z**2)")
eq=eq.replace("Derivative[2][P][z]","(2-3*theta**2*z)")
eq=eq.replace("I","1j")

"Replace of square root"

while 1:
    sqin=eq.find("Sqrt[")
    if sqin==-1:
        break
    endtest=0
    sqout=0
    j=sqin+5
    for i in range(len(eq)-(sqin+5)):
        if eq[j]=="[":
            endtest+=1
        if eq[j] == "]":
            endtest -= 1
        if endtest==-1:
            sqout=j
            # print(eq[j], j, endtest)
            break
        # print(eq[j], j, endtest)
        j+=1
    eq=eq[:sqout]+")**(0.5)"+eq[(sqout+1):]
    eq=eq[:sqin]+"("+eq[(sqin+5):]
    # print(eq)
print(eq)
"Write on file"

eq=re.sub('[\{\}]',"",eq)
eq=re.sub(r'([\w|\)\]]),([-\w|\(\[])',r'\1\n\n\2', eq)
file = open(write_name, "w")
file.write(eq)
file.close()

"TEST"
z=1.2*np.ones((7,7))
Psi=np.ones((7,7))
Spinor=0.9*np.ones((7,7,7))
DSpinor=0.9*np.ones((7,7,7,7))
Fields=0.9*np.ones((7,7,7))
DFields=np.ones((7,7,7,7))
k0=1
K=1
theta=1
L=1
m=1
q=1
file = open(write_name, "r")
eq_test=file.read()
file.close()
y=eq_test.split("\n\n")
for i in range(7):
    print(eval(y[i]))
