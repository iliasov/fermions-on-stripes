import re
import numpy as np

file = open("EOMsT_mod.txt", "r")
eq = file.read()
file.close()
eq=re.sub('\s',"",eq)
"Replace of the first derivatives"
eq=eq.replace("Derivative[0,1][Fx[[1]]][x,z]","DFields[:, 0, 6, 3]")
eq=eq.replace("Derivative[0,1][Fx[[2]]][x,z]","DFields[:, 0, 5, 3]")
eq=eq.replace("Derivative[0,1][Fx[[3]]][x,z]","DFields[:, 0, 0, 3]")
eq=eq.replace("Derivative[0,1][Fx[[4]]][x,z]","DFields[:, 0, 1, 3]")
eq=eq.replace("Derivative[0,1][Fx[[5]]][x,z]","DFields[:, 0, 2, 3]")
eq=eq.replace("Derivative[0,1][Fx[[6]]][x,z]","DFields[:, 0, 3, 3]")
eq=eq.replace("Derivative[0,1][Fx[[7]]][x,z]","DFields[:, 0, 4, 3]")

eq=eq.replace("Derivative[1,0][Fx[[1]]][x,z]","DFields[:, 0, 6, 1]")
eq=eq.replace("Derivative[1,0][Fx[[2]]][x,z]","DFields[:, 0, 5, 1]")
eq=eq.replace("Derivative[1,0][Fx[[3]]][x,z]","DFields[:, 0, 0, 1]")
eq=eq.replace("Derivative[1,0][Fx[[4]]][x,z]","DFields[:, 0, 1, 1]")
eq=eq.replace("Derivative[1,0][Fx[[5]]][x,z]","DFields[:, 0, 2, 1]")
eq=eq.replace("Derivative[1,0][Fx[[6]]][x,z]","DFields[:, 0, 3, 1]")
eq=eq.replace("Derivative[1,0][Fx[[7]]][x,z]","DFields[:, 0, 4, 1]")

"Replace of the second derivatives"
eq=eq.replace("Derivative[0,2][Fx[[1]]][x,z]","DDFields[:, 0, 6, 3, 3]")
eq=eq.replace("Derivative[0,2][Fx[[2]]][x,z]","DDFields[:, 0, 5, 3, 3]")
eq=eq.replace("Derivative[0,2][Fx[[3]]][x,z]","DDFields[:, 0, 0, 3, 3]")
eq=eq.replace("Derivative[0,2][Fx[[4]]][x,z]","DDFields[:, 0, 1, 3, 3]")
eq=eq.replace("Derivative[0,2][Fx[[5]]][x,z]","DDFields[:, 0, 2, 3, 3]")
eq=eq.replace("Derivative[0,2][Fx[[6]]][x,z]","DDFields[:, 0, 3, 3, 3]")
eq=eq.replace("Derivative[0,2][Fx[[7]]][x,z]","DDFields[:, 0, 4, 3, 3]")

eq=eq.replace("Derivative[2,0][Fx[[1]]][x,z]","DDFields[:, 0, 6, 1, 1]")
eq=eq.replace("Derivative[2,0][Fx[[2]]][x,z]","DDFields[:, 0, 5, 1, 1]")
eq=eq.replace("Derivative[2,0][Fx[[3]]][x,z]","DDFields[:, 0, 0, 1, 1]")
eq=eq.replace("Derivative[2,0][Fx[[4]]][x,z]","DDFields[:, 0, 1, 1, 1]")
eq=eq.replace("Derivative[2,0][Fx[[5]]][x,z]","DDFields[:, 0, 2, 1, 1]")
eq=eq.replace("Derivative[2,0][Fx[[6]]][x,z]","DDFields[:, 0, 3, 1, 1]")
eq=eq.replace("Derivative[2,0][Fx[[7]]][x,z]","DDFields[:, 0, 4, 1, 1]")

eq=eq.replace("Derivative[1,1][Fx[[1]]][x,z]","DDFields[:, 0, 6, 1, 3]")
eq=eq.replace("Derivative[1,1][Fx[[2]]][x,z]","DDFields[:, 0, 5, 1, 3]")
eq=eq.replace("Derivative[1,1][Fx[[3]]][x,z]","DDFields[:, 0, 0, 1, 3]")
eq=eq.replace("Derivative[1,1][Fx[[4]]][x,z]","DDFields[:, 0, 1, 1, 3]")
eq=eq.replace("Derivative[1,1][Fx[[5]]][x,z]","DDFields[:, 0, 2, 1, 3]")
eq=eq.replace("Derivative[1,1][Fx[[6]]][x,z]","DDFields[:, 0, 3, 1, 3]")
eq=eq.replace("Derivative[1,1][Fx[[7]]][x,z]","DDFields[:, 0, 4, 1, 3]")

"Replace of Fx's"

eq=eq.replace("Fx[[1]][x,z]","Fields[:, 0, 6]")
eq=eq.replace("Fx[[2]][x,z]","Fields[:, 0, 5]")
eq=eq.replace("Fx[[3]][x,z]","Fields[:, 0, 0]")
eq=eq.replace("Fx[[4]][x,z]","Fields[:, 0, 1]")
eq=eq.replace("Fx[[5]][x,z]","Fields[:, 0, 2]")
eq=eq.replace("Fx[[6]][x,z]","Fields[:, 0, 3]")
eq=eq.replace("Fx[[7]][x,z]","Fields[:, 0, 4]")

"Replace of other symbols"
eq=eq.replace("^","**")
eq=eq.replace("\[Mu]1","theta")
eq=eq.replace("Pi","np.pi")

"Write on file"

eq=re.sub('[\{\}]',"",eq)
eq=re.sub(r'([\w|\)\]]),([-\w|\(\[])',r'\1\n\n\2', eq)
file = open("Hor_bound_parsed.txt", "w")
file.write(eq)
file.close()

"TEST"
Fields=np.ones((7,7,7))
DFields=np.ones((7,7,7,7))
DDFields=np.ones((7,7,7,7,7))
k0=1
theta=1
file = open("Hor_bound_parsed.txt", "r")
eq_test=file.read()
file.close()
y=eq_test.split("\n\n")
for i in range(7):
    print(eval(y[i]))
