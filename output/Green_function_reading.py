import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import AdS_equations_bound as AdS
import AdS_equations_norm as AdS_norm
from mpl_toolkits.axes_grid1 import make_axes_locatable, axes_size
import matplotlib.ticker as ticker


from matplotlib.colors import LogNorm

def sol_Green_func(name):
    file = open(name, "r")
    solution = file.read()
    # x="[1,2]"
    solution = solution.replace("\n", "")
    solution = solution.replace("[", "")
    solution = solution.replace("]", "")
    solution = [complex(s) for s in solution.split()]
    # print(len(solution))
    # solution=eval('['+solution+']')
    # print(1)
    # print(solution)
    type=int(solution[0].real)
    N=int(solution[1].real)
    N1=int(solution[2].real)
    N2=int(solution[3].real)
    theta=solution[4].real
    k0=solution[5].real
    add_par=solution[6].real
    iter=7
    array_1=np.zeros(N1)
    array_2=np.zeros(N2)
    for i in range(N1):
        array_1[i] = solution[iter+i].real
    iter+=N1
    for i in range(N2):
        array_2[i] = solution[iter+i].real
    iter+=N2
    Gr = np.zeros(N * N1 * N2 * 4, dtype=complex)
    for i in range(N * N1 * N2 * 4):
        Gr[i] = solution[i+iter]

    Gr = Gr.reshape((N, N2, N1, 4))
    file.close()
    return type, N, N1, N2, theta, k0, array_1, array_2, add_par, Gr



def GR_view(type,N, N1, N2, theta, k0, array_1, array_2, add_par, GR_x, save=False, sum=False, typepicture=False, savename=""):

    print("type", type, "N", N, "N1", N1, "N2", N2, "theta", theta, "k0", k0, "add_par", add_par)
    GR_kx = np.fft.fft(GR_x, axis=0)
    GR_final = np.zeros((N2, N1, 4), dtype=complex)

    if not sum:
        GR_final.real += GR_kx[0, ...].real / N
        GR_final.imag += GR_kx[0, ...].imag / N

    if sum:
        for i in range(N):
            #GR_final += GR_kx[i, ...] / N
            GR_final.real += GR_kx[i, ...].real / N
            GR_final.imag += GR_kx[i, ...].imag / N



    "Image output"

    #tpl=abs(GR_kxfull_sort[:,0,:,0])**(0.5)
    if typepicture==1:
        tpl=GR_final[...,0].imag
    elif typepicture==2:
        tpl = GR_final[..., 0].real
    elif typepicture==3:
        tpl = GR_final[..., 1].imag
    elif typepicture==4:
        tpl = GR_final[..., 1].real
    elif typepicture==5:
        tpl=GR_final[...,2].imag
    elif typepicture==6:
        tpl = GR_final[..., 2].real
    elif typepicture==7:
        tpl = GR_final[..., 3].imag
    elif typepicture==8:
        tpl = GR_final[..., 3].real
    if typepicture=="DoS":
        tpl = (GR_final[..., 1] + GR_final[..., 2]).imag
        print(tpl.min())

    plt.figure()
    ax = plt.gca()
    im = plt.imshow(tpl.transpose(), interpolation='bilinear', extent=(min(array_2), max(array_2), min(array_1), max(array_1)),
                    cmap='seismic',
                    origin='lower',
                    vmax=tpl.max(), vmin=tpl.min())
                    #norm=LogNorm(vmin=0.01, vmax=abs(tpl).max()))


    plt.colorbar(im, shrink=.7)
    if type==-1:
        plt.xlabel('$\omega$')
        plt.ylabel('$k_{x}$')
        plt.title('$\mu$=' + str(theta) + " k0=" + str(k0)+' $k_{y}$='+str(add_par))
    if type==-2:
        plt.xlabel('$k_{x}$')
        plt.ylabel('$k_{y}$')
        ax.xaxis.set_major_formatter(ticker.FormatStrFormatter('%0.2f'))
        plt.title('$\mu$='+str(theta)+" k0="+str(k0)+' $\omega$='+str(add_par))

    
    if not save:
        plt.show()
    if save:
        plt.savefig(savename)



name="GR_Stripes_tau=1_Qlat_N=20 A0=5.0_theta=2.3_m=0_q=1.5 omega=0.0_Neps=15_k0=0.6"
figname="GR_Stripes_tau=1_Qlat_N=20 A0=5.0_theta=2.3_m=0_q=1.5 omega=0.0_Neps=15_k0=0.6.png"
type,N, N1, N2, theta, k0, array_1, array_2, add_par, GR_x=\
    sol_Green_func( name+".txt")
GR_view(type,N, N1, N2, theta, k0, array_1, array_2, add_par, GR_x, save=True, sum=False, typepicture="DoS",savename=figname)
