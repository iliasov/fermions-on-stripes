import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
#import AdS_equations_bound as AdS
import AdS_equations_norm_QS_trans as AdS_norm
import glob
import re


def sol_stripes(N, name):
    file=open(name, "r")
    solution=file.read()
    # x="[1,2]"
    solution=solution.replace("\n","")
    solution=solution.replace("[","")
    solution=solution.replace("]","")
    solution=[float(s) for s in solution.split()]

    # solution=eval('['+solution+']')
    # print(1)
    # print(solution)
    sol=np.zeros(N*N*8)
    for i in range(N*N*8):
        sol[i]=solution[i]

    sol=sol.reshape((N,N,8))
    file.close()
    return sol

def sol_R_tr(N, name):
    file=open(name, "r")
    solution=file.read()
    # x="[1,2]"
    solution=solution.replace("\n","")
    solution=solution.replace("[","")
    solution=solution.replace("]","")
    solution=[float(s) for s in solution.split()]

    # solution=eval('['+solution+']')
    # print(1)
    # print(solution)
    sol=np.zeros(N*N)
    for i in range(N*N):
        sol[i]=solution[i]

    sol=sol.reshape((N,N))
    file.close()
    return sol

def Rfig_in_dir():
    list=glob.glob("R_tr_Stripes_Qlat_N=20*.txt")
    for title in list:
        print(title+" is processed")
        R_tr = sol_R_tr(N, title)
        nl=len(title)
       # show_result(R_tr,"Results/"+title[:(nl-4)]+".png")
        show_result(R_tr, title[:(nl - 4)] + ".png")


def sol_Dirac(N, name):
    file = open(name, "r")
    solution = file.read()
    # x="[1,2]"
    solution = solution.replace("\n", "")
    solution = solution.replace("[", "")
    solution = solution.replace("]", "")
    solution = [complex(s) for s in solution.split()]

    # solution=eval('['+solution+']')
    # print(1)
    # print(solution)
    sol = np.zeros(N * N * 4, dtype=complex)
    for i in range(N * N * 4):
        sol[i] = solution[i]

    sol = sol.reshape((N, N, 4))
    file.close()
    return sol

def sol_Green_func(N, name):
    file = open(name, "r")
    solution = file.read()
    # x="[1,2]"
    solution = solution.replace("\n", "")
    solution = solution.replace("[", "")
    solution = solution.replace("]", "")
    solution = [complex(s) for s in solution.split()]

    # solution=eval('['+solution+']')
    # print(1)
    # print(solution)
    sol = np.zeros(N * N * 4, dtype=complex)
    for i in range(N * N * 4):
        sol[i] = solution[i]

    sol = sol.reshape((N, N, 4))
    file.close()
    return sol

def Rev_irrev(N):

    Fields_hor=np.zeros((8,30))
    k=np.zeros(30)
    for i in range(30):

        k0=round(0.6+i*0.05,2)
        k[i]=k0
        Fields = sol_stripes(N, "Stripes_QS_trans_N=20_A0=1.5_k0="+str(k0)+"_theta=2.3_tau=0.1_M(imag)=1.41.txt")
        #Fields_hor[:, i]=Fields[16,0,:]
        for j in range(N):
            Fields_hor[:,i]+=np.abs(Fields[j,0,:])/N
        #Fields_hor=Fields_hor/N

    fig, ax = plt.subplots()

    ax.plot(k, Fields_hor[0,:],label='$Q_{tt}$ and $Q_{zz}$')
    ax.plot(k, Fields_hor[1,:],label='$Q_{xx}$')
    ax.plot(k, Fields_hor[2,:],label='$Q_{yy}$')
    #ax.plot(k, Fields_hor[3,:],label='Q_{zz}')
    ax.plot(k, Fields_hor[5,:],label='Electric potential')
    ax.plot(k, Fields_hor[6,:],label='Scalar field')
    legend = ax.legend()
    plt.xlabel("$k_{x}$")

    plt.show()

def metric_pic(N, filename, savename):

    #Fields_hor=np.zeros((8,30))

    Fields = sol_stripes(N, filename)

    fig, ax = plt.subplots()

    z=np.zeros(N)
    z_end = 1
    z_start = 0
    for i in range(N):  # сетка по оси z
        z[i] = 0.5 * (z_end + z_start) + 0.5 * (z_end - z_start) * np.cos(np.pi * i / (N-1))

    ax.plot(z, Fields[0,:,0],label='$Q_{tt}$ and $Q_{zz}$')
    ax.plot(z, Fields[0,:,1],label='$Q_{xx}$')
    ax.plot(z, Fields[0,:,2],label='$Q_{yy}$')
    #ax.plot(k, Fields_hor[3,:],label='Q_{zz}')
    ax.plot(z, Fields[0,:,5],label='Electric potential')
    ax.plot(z, Fields[0,:,6],label='Scalar field')
    legend = ax.legend()
    plt.xlabel("z$")
    plt.savefig(savename)
    plt.close()
    #plt.show()

def all_metric_pics():

    list = glob.glob("Stripes_QS_trans_N=20*tau=1_M(imag)=1.41.txt")
    for title in list:
        rr = re.findall("[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?", title)
        print(title + " is processed")
        print(title[:-3]+"png")
        metric_pic(N, title, title[:-3]+"png")

def show_result(sol, savename, save=True):

    Tx = 2 * np.pi  # lattice period
    delta_x = Tx / N
    x = np.zeros(N)
    z = np.zeros(N)
    for i in range(N):  # x grid
        x[i] = delta_x * i

    epsilon = 0.01
    z_end = 1
    z_start = 0
    for i in range(N):  # z grid
        z[i] = 0.5 * (z_end + z_start) + 0.5 * (z_end - z_start) * np.cos(np.pi * i / (N-1))
    tpl = sol
    # tpl=Fields[...,4]
    fig, ax = plt.subplots()
    im = plt.imshow(tpl, interpolation='bilinear', cmap=cm.RdYlGn,
                    origin='lower', aspect='auto', extent=(max(z), min(z), min(x), max(x)),
                    vmax=abs(tpl).max(), vmin=-abs(tpl).max())
    plt.colorbar()
    plt.xlabel('$z$')
    plt.ylabel('$x$')

    # plt.plot(sol[:,N-1,0].imag)

    if not save:
        plt.show()
    if save:
        plt.savefig(savename)
        plt.close()





N=20

"Image output"
show_result(Fields[...,0], "Qzz_Stripes_QS_trans_N=20_A0=1.5_k0=2.2_theta=2.3_tau=0.0_M(imag)=1.41.png", save=False)




