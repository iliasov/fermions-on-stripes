import numpy as np

def a(y, k , N):  # is used in z-derivative matrix
    P = 1
    for i in range(N):
        if i == k:
            continue
        P *= (y[i] - y[k])
    return P

def D_num_func( N, delta_x, k0, z):  # Calculation of derivative matrixes
    D_num=np.zeros((N,N,4))
    # z-derivative matrix
    for j in range(N):
        for k in range(N):
            if k == j:
                continue
            D_num[j, j, 3] += 1 / (z[j] - z[k])
    for i in range(N):
        for j in range(N):
            if i == j:
                continue
            D_num[i, j, 3] = a(z, j, N) / (a(z, i, N) * (z[j] - z[i]))  # with taking transponse into account
    # x-derivative matrix
    y = np.zeros(N)
    y[N-1] = 0
    for i in range(N - 1):
        y[i] = 0.5 * k0*(-1) ** (i + 1)*np.cos((i + 1) * delta_x / 2)/ np.sin((i + 1) * delta_x / 2)
    for i in range(N):

        D_num[:(N - i), N-(i+1), 1] = y[i:]  # cyclic permutations
        D_num[(N - i):, N-(i+1), 1] = y[:i]
    return D_num

def Diff(D_num, Ts, index):  # function calculating derivatives
    DTs = np.zeros(index)
    DTs[..., 1] = np.einsum(D_num[..., 1], [0, 1], Ts, [1, 2, Ellipsis], [0, 2, Ellipsis]) #x-derivative
    DTs[..., 3] = np.einsum(Ts, [0, 1, Ellipsis], D_num[..., 3], [1, 2], [0, 2, Ellipsis]) #z-derivative
    return DTs

def P(z, theta):
    return 1+(1-z**2)+(1-z**2)**2-(theta**2*(1-z**2)**3)/2

def DP(z, theta):
    return -2*z*(1+2*(1-z**2)-1.5*theta**2*(1-z**2)**2)

def Dirac_normed(Fields, Spinor, N, K, ky, omega, k0, theta, m, q, epsilon, basevector):

    #print("N=", N, " K=", K, " ky=", ky, " omega=", omega, "k0=", k0, " theta=", theta, " m=", m, " q=", q, "epsilon=", epsilon)
    Spinor=Spinor.reshape((N,N,4))
    z_1 = np.zeros(N)
    z = np.ones((N, N))
    z_end = 1 - epsilon
    z_start = epsilon
    for i in range(N):  # z axis grid
        z_1[i] = 0.5 * (z_end + z_start) + 0.5 * (z_end - z_start) * np.cos(np.pi * i / (N-1))
    for i in range(N):
        z[i, :] = z_1
    Tx = 2 * np.pi / k0  # period of modulation
    delta_x = Tx / N

    D_num = D_num_func(N, k0 * delta_x, k0, z_1)
    DFields = Diff(D_num, Fields, (N, N, 7, 4))  # first derivative of the fields
    DDFields = Diff(D_num, DFields, (N, N, 7, 4, 4))  # the second derivative of the metric parts and the fields
    DSpinor = np.zeros((N, N, 4, 4), dtype=complex)
    # DSpinor[..., 0] = -1j * omega * Spinor
    DSpinor[..., 1] = np.einsum(D_num[..., 1], [0, 1], Spinor, [1, 2, Ellipsis], [0, 2, Ellipsis])
    #DSpinor[...,1]=0.3*1j*Spinor
    #DSpinor[..., 2] = 1j * ky * Spinor
    DSpinor[..., 3] = np.einsum(Spinor, [0, 1, Ellipsis], D_num[..., 3], [1, 2], [0, 2, Ellipsis])
    #print("dFields test", np.max(abs(DDFields)))
    #Psi = np.zeros((N,N))
    Psi=z**2*Fields[...,5]
    #Psi=z**2*theta
    P_z=P(z,theta)
    P_0=P(0,theta)
    DP_0=DP(0,theta)

    "Dirac Equations"

    result=np.zeros((N,N,4), dtype=complex)

    #AdS-RN

    # result[..., 0] = (q * z ** 2 * theta + omega) * Spinor[..., 1] + np.sqrt(P(z, theta)) * (
    # -K * z * Spinor[..., 1] + ky * z * Spinor[..., 3]) + P(z, theta) * (
    # -((1j * omega * Spinor[..., 0]) / P(0, theta)) + 1 / 2 * z * DSpinor[..., 0, 3])
    # result[..., 1] = (q * z ** 2 * theta + omega + K * z * np.sqrt(P(z, theta))) * Spinor[..., 0] - ky * z * np.sqrt(
    #     P(z, theta)) * Spinor[..., 2] + P(z, theta) * (
    # (1j * omega * Spinor[..., 1]) / P(0, theta) - 1 / 2 * z * DSpinor[..., 1, 3])
    # result[..., 2] = (q * z ** 2 * theta + omega) * Spinor[..., 3] + z * np.sqrt(P(z, theta)) * (
    # ky * Spinor[..., 1] + K * Spinor[..., 3]) + P(z, theta) * (
    # -((1j * omega * Spinor[..., 2]) / P(0, theta)) + 1 / 2 * z * DSpinor[..., 2, 3])
    # result[..., 3] = (q * z ** 2 * theta + omega) * Spinor[..., 2] - z * np.sqrt(P(z, theta)) * (
    # ky * Spinor[..., 0] + K * Spinor[..., 2]) + P(z, theta) * (
    # (1j * omega * Spinor[..., 3]) / P(0, theta) - 1 / 2 * z * DSpinor[..., 3, 3])

    #Stripes

    result[...,0]=2*Fields[..., 2]*(2*(1-z**2)**(1+2*m)*(omega+q*Psi)*P_0*(P_z*Fields[..., 0]*Fields[..., 1]**3)**(0.5)*Fields[..., 3]*Spinor[..., 1]+Fields[..., 0]*(P_z**3*Fields[..., 1]**3*Fields[..., 3])**(0.5)*(Spinor[..., 0]*((-2*1j)*omega+(2*1j)*z**2*omega+2*m*z**2*P_0-(2*1j)*K*z**2*(-1+z**2)**3*P_0*Fields[..., 4]-z**2*(-1+z**2)**3*P_0*DFields[..., 4, 1])-z*(-1+z**2)*P_0*(DSpinor[..., 0, 3]+2*z*(-1+z**2)**2*Fields[..., 4]*DSpinor[..., 0, 1])))+z*P_0*P_z*Fields[..., 0]*(-4*m*Fields[..., 1]**(3/2)*Fields[..., 2]*Fields[..., 3]*Spinor[..., 0]+(1-z**2)**(1+2*m)*Fields[..., 3]*(4*ky*(Fields[..., 1]**3*Fields[..., 2])**(0.5)*Spinor[..., 3]-1j*Fields[..., 2]*Spinor[..., 1]*DFields[..., 1, 1])-(1-z**2)**(1+2*m)*Fields[..., 1]*Fields[..., 2]*((-1j)*Spinor[..., 1]*DFields[..., 3, 1]+4*Fields[..., 3]*(K*Spinor[..., 1]-1j*DSpinor[..., 1, 1])))

    result[...,1]=(2*z*(4*(-1+z**2)*(omega+q*Psi)*P_0*(P_z*Fields[..., 0])**(3/2)*Fields[..., 1]**2*Fields[..., 2]*Fields[..., 3]*Spinor[..., 0]+z*P_0*P_z**2*Fields[..., 0]**2*(Fields[..., 1]*Fields[..., 2])**(0.5)*(-4*ky*(-1+z**2)*Fields[..., 1]**(3/2)*Fields[..., 3]*Spinor[..., 2]+Fields[..., 3]*(4*m*(1-z**2)**(2*m)*(Fields[..., 1]**3*Fields[..., 2])**(0.5)*Spinor[..., 1]+1j*(-1+z**2)*(Fields[..., 2])**(0.5)*Spinor[..., 0]*DFields[..., 1, 1])+(-1+z**2)*Fields[..., 1]*(Fields[..., 2])**(0.5)*((-1j)*Spinor[..., 0]*DFields[..., 3, 1]+4*Fields[..., 3]*(K*Spinor[..., 0]-1j*DSpinor[..., 0, 1])))+2*(1-z**2)**(2*m)*Fields[..., 0]**2*Fields[..., 1]**2*Fields[..., 2]*(P_z**5*Fields[..., 3])**(0.5)*(Spinor[..., 1]*((2*1j)*(-1+z**2)*omega-2*m*z**2*P_0-(2*1j)*K*z**2*(-1+z**2)**3*P_0*Fields[..., 4]-z**2*(-1+z**2)**3*P_0*DFields[..., 4, 1])-z*(-1+z**2)*P_0*(DSpinor[..., 1, 3]+2*z*(-1+z**2)**2*Fields[..., 4]*DSpinor[..., 1, 1]))))/(-1+z**2)

    result[...,2]=8*(1-z**2)**(1+2*m)*(omega+q*Psi)*P_0*(P_z*Fields[..., 0]*Fields[..., 1]*Fields[..., 2])**(3/2)*Fields[..., 3]**2*Spinor[..., 3]+4*P_z**(5/2)*Fields[..., 0]**2*(Fields[..., 1]*Fields[..., 2]*Fields[..., 3])**(3/2)*(Spinor[..., 2]*((-2*1j)*omega+(2*1j)*z**2*omega+2*m*z**2*P_0-(2*1j)*K*z**2*(-1+z**2)**3*P_0*Fields[..., 4]-z**2*(-1+z**2)**3*P_0*DFields[..., 4, 1])-z*(-1+z**2)*P_0*(DSpinor[..., 2, 3]+2*z*(-1+z**2)**2*Fields[..., 4]*DSpinor[..., 2, 1]))-2*z*P_0*P_z**2*Fields[..., 0]**2*Fields[..., 2]*Fields[..., 3]*(-4*ky*(1-z**2)**(1+2*m)*Fields[..., 1]**(3/2)*Fields[..., 3]*Spinor[..., 1]+Fields[..., 3]*(4*m*(Fields[..., 1]**3*Fields[..., 2])**(0.5)*Spinor[..., 2]-1j*(1-z**2)**(1+2*m)*(Fields[..., 2])**(0.5)*Spinor[..., 3]*DFields[..., 1, 1])-(1-z**2)**(1+2*m)*Fields[..., 1]*(Fields[..., 2])**(0.5)*((-1j)*Spinor[..., 3]*DFields[..., 3, 1]+4*Fields[..., 3]*(K*Spinor[..., 3]-1j*DSpinor[..., 3, 1])))

    result[...,3]=2*z*P_z*Fields[..., 0]*(4*(omega+q*Psi)*P_0*Fields[..., 1]**2*(P_z*Fields[..., 0]*Fields[..., 2]**3*Fields[..., 3]**3)**(0.5)*Spinor[..., 2]-1j*z*P_0*P_z*Fields[..., 0]*Spinor[..., 2]*((Fields[..., 1]*Fields[..., 2]**3*Fields[..., 3]**3)**(0.5)*DFields[..., 1, 1]-(Fields[..., 1]**3*Fields[..., 2]**3*Fields[..., 3])**(0.5)*DFields[..., 3, 1])-4*z*P_0*P_z*Fields[..., 0]*(Fields[..., 1]*Fields[..., 2]*Fields[..., 3])**(3/2)*(K*Spinor[..., 2]-1j*DSpinor[..., 2, 1])-(2*P_z*Fields[..., 0]*Fields[..., 1]**2*Fields[..., 3]*(2*ky*z*(-1+z**2)*P_0*Fields[..., 2]*(Fields[..., 3])**(0.5)*Spinor[..., 0]+(1-z**2)**(2*m)*(Spinor[..., 3]*((2*1j)*omega*(P_z*Fields[..., 2]**3)**(0.5)-(2*1j)*z**2*omega*(P_z*Fields[..., 2]**3)**(0.5)+2*m*z**2*P_0*(P_z*Fields[..., 2]**3)**(0.5)+(2*1j)*K*z**2*(-1+z**2)**3*P_0*Fields[..., 4]*(P_z*Fields[..., 2]**3)**(0.5)-2*m*z*P_0*(Fields[..., 2]**3*Fields[..., 3])**(0.5)+z**2*(-1+z**2)**3*P_0*(P_z*Fields[..., 2]**3)**(0.5)*DFields[..., 4, 1])+z*(-1+z**2)*P_0*(P_z*Fields[..., 2]**3)**(0.5)*(DSpinor[..., 3, 3]+2*z*(-1+z**2)**2*Fields[..., 4]*DSpinor[..., 3, 1]))))/(-1+z**2))


    # "Boundary conditions"

    
    result[:, N - 1, 1] = Spinor[:, N - 1, 1] - 1j * Spinor[:, N - 1, 0]
    result[:, N - 1, 3] = Spinor[:, N - 1, 3] - 1j * Spinor[:, N - 1, 2]

    result[...,N-1,0]=(DSpinor[:,N-1, 0, 3]-1j*DSpinor[:,N-1, 1, 3])*P_0**2*(Fields[:,N-1, 1])**(0.5)*Fields[:,N-1, 3]+2*(P_0*Fields[:,N-1, 3])**(3/2)*(DSpinor[:,N-1, 0, 1]+1j*DSpinor[:,N-1, 1, 1])-4*omega*Spinor[:,N-1, 1]*(Fields[:,N-1, 1])**(0.5)*Fields[:,N-1, 3]*DP_0+2*omega*Spinor[:,N-1, 1]*P_0*(Fields[:,N-1, 1])**(0.5)*(-DFields[:,N-1, 0, 3]+DFields[:,N-1, 3, 3])

    result[...,N-1,2]=(DSpinor[:,N-1, 2, 3]-1j*DSpinor[:,N-1, 3, 3])*P_0**2*(Fields[:,N-1, 1])**(0.5)*Fields[:,N-1, 3]-2*(P_0*Fields[:,N-1, 3])**(3/2)*(DSpinor[:,N-1, 2, 1]+1j*DSpinor[:,N-1, 3, 1])-4*omega*Spinor[:,N-1, 3]*(Fields[:,N-1, 1])**(0.5)*Fields[:,N-1, 3]*DP_0+2*omega*Spinor[:,N-1, 3]*P_0*(Fields[:,N-1, 1])**(0.5)*(-DFields[:,N-1, 0, 3]+DFields[:,N-1, 3, 3])

    if basevector==0:
        #Conformal boundary
        result [:,0,0]=Spinor[:,0,0]
        result[:,0, 2] =Spinor[:,0,2]-1
        

    if basevector == 1:
        #Conformal boundary
        result [:,0,0]=Spinor[:,0,0]-1
        result[:,0, 2] =Spinor[:,0,2]
        

    result=result.reshape(N*N*4)

    return result