import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import sys



def coord_transform(z):
    return 1-z**2

def to_new_lattice(old_lattice, new_lattice, func_values):

    aux_lattice = coord_transform(old_lattice)
    print(aux_lattice)
    if min(new_lattice) < min(aux_lattice) or max(new_lattice) > max(aux_lattice):
        print (min(new_lattice), max(new_lattice), min(aux_lattice), max(aux_lattice), 'Beyond interpolation limit')
        sys.exit()

    interpolation = interp1d(aux_lattice, func_values, kind='cubic')
    result = interpolation(new_lattice)

    return result

def define_Fields(N, name, AdS=False):
    if not AdS:
        file=open(name, "r")
        solution=file.read()
        # x="[1,2]"
        solution=solution.replace("\n","")
        solution=solution.replace("[","")
        solution=solution.replace("]","")
        solution=[float(s) for s in solution.split()]

        # solution=eval('['+solution+']')
        # print(1)
        # print(solution)
        sol=np.zeros(N*N*7)
        for i in range(N*N*7):
            sol[i]=solution[i]

        sol=sol.reshape((N,N,7))
        file.close()
        return sol

def save_interpolate_Fields(N, name):

    z_1 = np.zeros(N)
    z_end = 1
    z_start = 0
    for i in range(N):  # сетка по оси z
        z_1[i] = 0.5 * (z_end + z_start) + 0.5 * (z_end - z_start) * np.cos(np.pi * i / (N-1))

    for i in range(N):
        transformed_function[:,i] = to_new_lattice(old_latt, desired_latt, func_val[:,i])

    file = open(name, "r")
    solution = file.read()
    # x="[1,2]"
    solution = solution.replace("\n", "")
    solution = solution.replace("[", "")
    solution = solution.replace("]", "")
    solution = [float(s) for s in solution.split()]

    # solution=eval('['+solution+']')
    # print(1)
    # print(solution)
    sol = np.zeros(N * N * 7)
    for i in range(N * N * 7):
        sol[i] = solution[i]

    sol = sol.reshape((N, N, 7))
    file.close()
    return sol

# transformed_function=np.zeros((N,N))
# for i in range(N):
#     transformed_function[:,i] = to_new_lattice(old_latt, desired_latt, func_val[:,i])
#
# plt.plot(old_latt,func_val[:,5],'o',desired_latt, transformed_function[:,5], 'o')
# plt.legend(['original', 'transformed'], loc='best')
# plt.show()
