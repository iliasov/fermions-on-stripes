import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
# import AdS_equations_bound as AdS
from scipy.optimize import root
#from AdS_equations_bound import AdS_stripes, DeTurktensor_test, DeTurk_test
#from Dirac_eq import Dirac_solve, background_solve, D_num_func, Diff
from Dirac_norm import Dirac_normed
from AdS_equations_norm import AdS_stripes_norm, xi_norm



def Jacobian_Dirac( Sp_con, gUU, verbein, gamma, L_gen, Bloch, Psi, D_num, Spinor, N, K, ky, omega, m, q):

    eps=1
    Jac=np.zeros((N**2*4,N**2*4), dtype=complex)
    Result_0=Dirac_solve(Sp_con, gUU, verbein, gamma, L_gen, Bloch, Psi, D_num, Spinor, N, K, ky, omega, m, q)
    #print(np.shape(Spinor))
    for i in range(N**2*4):
        Spinor[i]+=eps
        Jac[:,i]=Dirac_solve(Sp_con, gUU, verbein, gamma, L_gen, Bloch, Psi, D_num, Spinor, N, K, ky, omega, m, q)-Result_0
        Spinor[i] -= eps
        # for j in range(N**2*7):
        #     if Jac[j,i]<eps:
        #         Jac[j,i]=0


    return Jac



def Newton_Dirac(Fields, Spinor_0, N, K, ky, omega, k0, theta, m, q, epsilon):

    x = Spinor_0
    Sp_con, gUU, verbein, gamma, L_gen, Bloch, Psi, D_num = background_solve(Fields, N, K, k0, theta, epsilon)
    # print("Sp_con",np.shape(Sp_con))
    # print("gUU", np.shape(gUU))
    # print("verbein", np.shape(verbein))
    # print("gamma", np.shape(gamma))
    # print("L_gen", np.shape(L_gen))
    # print("Bloch", np.shape(Bloch))
    # print("Psi", np.shape(Psi))
    # print("D_num", np.shape(D_num))
    Jac = Jacobian_Dirac(Sp_con, gUU, verbein, gamma, L_gen, Bloch, Psi, D_num, x, N, K, ky, omega, m, q)

    for i in range(2):
        y = Dirac_solve(Sp_con, gUU, verbein, gamma, L_gen, Bloch, Psi, D_num, x, N, K, ky, omega, m, q)
        tol = max(abs(y))
        print(tol)
        if tol < 10 ** (-6):
            break
        delta_x = np.linalg.solve(Jac, -y)
        # print('Fields', delta_x.reshape((N,N,7)))
        # print('Result', y.reshape((N,N,7)))
        x += delta_x

    return x

def Newton_Dirac_normed(Fields, Spinor_0, N, K, ky, omega, k0, theta, m, q, epsilon, basevector):

    x = Spinor_0
    Jac = Jacobian_Dirac_normed(Fields, N, K, ky, omega, k0, theta, m, q, epsilon)

    for i in range(2):
        y = Dirac_normed(Fields, x, N, K, ky, omega, k0, theta, m, q, epsilon, basevector)
        tol = max(abs(y))
        print(tol)
        if tol < 10 ** (-6):
            break
        delta_x = np.linalg.solve(Jac, -y)
        # print('Fields', delta_x.reshape((N,N,7)))
        # print('Result', y.reshape((N,N,7)))
        x += delta_x

    return x



def Jacobian_Dirac_normed(Fields, N, K, ky, omega, k0, theta, m, q, epsilon):

    # eps = 1
    # Jac = np.zeros((N ** 2 * 4, N ** 2 * 4), dtype=complex)
    # Result_0 = Dirac_normed(Fields, x, N, K, ky, omega, k0, theta, m, q, epsilon)
    # # print(np.shape(Spinor))
    # for i in range(N ** 2 * 4):
    #     x[i] += eps
    #     Jac[:, i] = Dirac_normed(Fields, x, N, K, ky, omega, k0, theta, m, q, epsilon) - Result_0
    #     x[i] -= eps
    #     # for j in range(N**2*7):
    #     #     if Jac[j,i]<eps:
    #     #         Jac[j,i]=0

    Jac = np.zeros((N ** 2, 4, N ** 2, 4), dtype=complex)
    XD = np.zeros((N ** 2, N ** 2), dtype=complex)
    ZD = np.zeros((N ** 2, N ** 2), dtype=complex)

    Tx = 2 * np.pi / k0  # modulation period
    delta_x = Tx / N
    z_1 = np.zeros(N)
    z_end = 1 - epsilon
    z_start = epsilon
    for i in range(N):  # z grid
        z_1[i] = 0.5 * (z_end + z_start) + 0.5 * (z_end - z_start) * np.cos(np.pi * i / (N-1))
    D_num = D_num_func(N, k0 * delta_x, k0, z_1)
    z = np.zeros((N, N))
    for i in range(N):
        z[i, :] = z_1

    #Psi = np.zeros((N, N))
    Psi = z**2*Fields[...,5]
    #Psi=z**2*theta
    P_z=P(z,theta)
    P_0=P(0,theta)
    DP_0=DP(0,theta)

    I = np.eye(N)
    # D_num_mod = D_num.copy()
    # D_num_mod[:, 0,3] = 0
    #D_num_mod[0, :, 1] = 0
    XD = np.einsum(I, [0, 2], D_num[..., 1], [1, 3], [1, 0, 3, 2]).reshape(N ** 2, N ** 2)
    ZD = np.einsum(I, [0, 2], D_num[...,3], [3, 1], [0, 1, 2, 3]).reshape(N ** 2, N ** 2)
    DFields = Diff(D_num, Fields, (N, N, 7, 4))

    "Jacobian coefficients"
    Xcoef = np.zeros((N, N, 4, 4), dtype=complex)
    Zcoef = np.zeros((N, N, 4, 4), dtype=complex)
    Fcoef = np.zeros((N, N, 4, 4), dtype=complex)


    Xcoef[...,0,0]=-4*z**2*(-1+z**2)**3*P_0*Fields[..., 0]*Fields[..., 4]*Fields[..., 2]*(P_z**3*Fields[..., 1]**3*Fields[..., 3])**(0.5)

    Xcoef[...,0,1]=(4*1j)*z*(1-z**2)**(1+2*m)*P_0*P_z*Fields[..., 0]*Fields[..., 1]*Fields[..., 2]*Fields[..., 3]

    Xcoef[...,1,0]=(-8*1j)*z**2*P_0*P_z**2*Fields[..., 0]**2*Fields[..., 1]**(3/2)*Fields[..., 2]*Fields[..., 3]

    Xcoef[...,1,1]=-8*z**3*(1-z**2)**(2*(1+m))*P_0*Fields[..., 0]**2*Fields[..., 1]**2*Fields[..., 4]*Fields[..., 2]*(P_z**5*Fields[..., 3])**(0.5)

    Xcoef[...,2,2]=-8*z**2*(-1+z**2)**3*P_0*P_z**(5/2)*Fields[..., 0]**2*Fields[..., 4]*(Fields[..., 1]*Fields[..., 2]*Fields[..., 3])**(3/2)

    Xcoef[...,2,3]=(-8*1j)*z*(1-z**2)**(1+2*m)*P_0*P_z**2*Fields[..., 0]**2*Fields[..., 1]*Fields[..., 2]**(3/2)*Fields[..., 3]**2

    Xcoef[...,3,2]=(8*1j)*z**2*P_0*P_z**2*Fields[..., 0]**2*(Fields[..., 1]*Fields[..., 2]*Fields[..., 3])**(3/2)

    Xcoef[...,3,3]=-8*z**3*(1-z**2)**(2*(1+m))*P_0*P_z**(5/2)*Fields[..., 0]**2*Fields[..., 1]**2*Fields[..., 4]*Fields[..., 2]**(3/2)*Fields[..., 3]

    Zcoef[...,0,0]=-2*z*(-1+z**2)*P_0*Fields[..., 0]*Fields[..., 2]*(P_z**3*Fields[..., 1]**3*Fields[..., 3])**(0.5)

    Zcoef[...,1,1]=-4*z**2*(1-z**2)**(2*m)*P_0*Fields[..., 0]**2*Fields[..., 1]**2*Fields[..., 2]*(P_z**5*Fields[..., 3])**(0.5)

    Zcoef[...,2,2]=-4*z*(-1+z**2)*P_0*P_z**(5/2)*Fields[..., 0]**2*(Fields[..., 1]*Fields[..., 2]*Fields[..., 3])**(3/2)

    Zcoef[...,3,3]=-4*z**2*(1-z**2)**(2*m)*P_0*P_z**(5/2)*Fields[..., 0]**2*Fields[..., 1]**2*Fields[..., 2]**(3/2)*Fields[..., 3]

    Fcoef[...,0,0]=2*Fields[..., 0]*Fields[..., 2]*(-2*m*z*P_0*P_z*Fields[..., 1]**(3/2)*Fields[..., 3]+(P_z**3*Fields[..., 1]**3*Fields[..., 3])**(0.5)*((-2*1j)*omega+(2*1j)*z**2*omega+2*m*z**2*P_0-(2*1j)*K*z**2*(-1+z**2)**3*P_0*Fields[..., 4]-z**2*(-1+z**2)**3*P_0*DFields[..., 4, 1]))

    Fcoef[...,0,1]=-((1-z**2)**(1+2*m)*P_0*Fields[..., 2]*(-4*(omega+q*Psi)*(P_z*Fields[..., 0]*Fields[..., 1]**3)**(0.5)*Fields[..., 3]+z*P_z*Fields[..., 0]*(1j*Fields[..., 3]*DFields[..., 1, 1]+Fields[..., 1]*(4*K*Fields[..., 3]-1j*DFields[..., 3, 1]))))

    Fcoef[...,0,3]=4*ky*z*(1-z**2)**(1+2*m)*P_0*P_z*Fields[..., 0]*(Fields[..., 1]**3*Fields[..., 2])**(0.5)*Fields[..., 3]

    Fcoef[...,1,0]=2*z*P_0*P_z*Fields[..., 0]*(Fields[..., 2])**(0.5)*(4*(omega+q*Psi)*Fields[..., 1]**2*(P_z*Fields[..., 0]*Fields[..., 2])**(0.5)*Fields[..., 3]+z*P_z*Fields[..., 0]*(Fields[..., 3]*(4*K*(Fields[..., 1]**3*Fields[..., 2])**(0.5)+1j*(Fields[..., 1]*Fields[..., 2])**(0.5)*DFields[..., 1, 1])-1j*(Fields[..., 1]**3*Fields[..., 2])**(0.5)*DFields[..., 3, 1]))

    Fcoef[...,1,1]=-4*z*(1-z**2)**(-1+2*m)*Fields[..., 0]**2*Fields[..., 1]**2*Fields[..., 2]*(2*m*z*P_0*P_z**2*Fields[..., 3]+(P_z**5*Fields[..., 3])**(0.5)*((2*1j)*(-1+z**2)*omega-2*m*z**2*P_0-(2*1j)*K*z**2*(-1+z**2)**3*P_0*Fields[..., 4]-z**2*(-1+z**2)**3*P_0*DFields[..., 4, 1]))

    Fcoef[...,1,2]=-8*ky*z**2*P_0*P_z**2*Fields[..., 0]**2*Fields[..., 1]**2*(Fields[..., 2])**(0.5)*Fields[..., 3]

    Fcoef[...,2,1]=8*ky*z*(1-z**2)**(1+2*m)*P_0*P_z**2*Fields[..., 0]**2*Fields[..., 1]**(3/2)*Fields[..., 2]*Fields[..., 3]**2

    Fcoef[...,2,2]=4*P_z**2*Fields[..., 0]**2*Fields[..., 2]*Fields[..., 3]*(-2*m*z*P_0*(Fields[..., 1]**3*Fields[..., 2])**(0.5)*Fields[..., 3]+(P_z*Fields[..., 1]**3*Fields[..., 2]*Fields[..., 3])**(0.5)*((-2*1j)*omega+(2*1j)*z**2*omega+2*m*z**2*P_0-(2*1j)*K*z**2*(-1+z**2)**3*P_0*Fields[..., 4]-z**2*(-1+z**2)**3*P_0*DFields[..., 4, 1]))

    Fcoef[...,2,3]=2*(1-z**2)**(1+2*m)*P_0*P_z*Fields[..., 0]*Fields[..., 2]*Fields[..., 3]*(4*(omega+q*Psi)*(P_z*Fields[..., 0]*Fields[..., 1]**3*Fields[..., 2])**(0.5)*Fields[..., 3]+z*P_z*Fields[..., 0]*(Fields[..., 2])**(0.5)*(1j*Fields[..., 3]*DFields[..., 1, 1]+Fields[..., 1]*(4*K*Fields[..., 3]-1j*DFields[..., 3, 1])))

    Fcoef[...,3,0]=-8*ky*z**2*P_0*P_z**2*Fields[..., 0]**2*Fields[..., 1]**2*Fields[..., 2]*Fields[..., 3]**(3/2)

    Fcoef[...,3,2]=2*z*P_0*P_z*Fields[..., 0]*(4*(omega+q*Psi)*Fields[..., 1]**2*(P_z*Fields[..., 0]*Fields[..., 2]**3*Fields[..., 3]**3)**(0.5)+z*P_z*Fields[..., 0]*(-4*K*(Fields[..., 1]*Fields[..., 2]*Fields[..., 3])**(3/2)-1j*(Fields[..., 1]*Fields[..., 2]**3*Fields[..., 3]**3)**(0.5)*DFields[..., 1, 1]+1j*(Fields[..., 1]**3*Fields[..., 2]**3*Fields[..., 3])**(0.5)*DFields[..., 3, 1]))

    Fcoef[...,3,3]=-4*z*(1-z**2)**(-1+2*m)*P_z**2*Fields[..., 0]**2*Fields[..., 1]**2*Fields[..., 3]*((-2*1j)*omega*(P_z*Fields[..., 2]**3)**(0.5)+(2*1j)*z**2*omega*(P_z*Fields[..., 2]**3)**(0.5)-2*m*z**2*P_0*(P_z*Fields[..., 2]**3)**(0.5)-(2*1j)*K*z**2*(-1+z**2)**3*P_0*Fields[..., 4]*(P_z*Fields[..., 2]**3)**(0.5)+2*m*z*P_0*(Fields[..., 2]**3*Fields[..., 3])**(0.5)-z**2*(-1+z**2)**3*P_0*(P_z*Fields[..., 2]**3)**(0.5)*DFields[..., 4, 1])

    ##AdS-RN

    # Zcoef[..., 0, 0] = 0.5*P(z,theta)*z
    #
    # Zcoef[..., 1, 1] = -0.5*P(z,theta)*z
    #
    # Zcoef[..., 2, 2] = 0.5*P(z,theta)*z
    #
    # Zcoef[..., 3, 3] = -0.5*P(z,theta)*z
    #
    # Fcoef[...,0,0]=-P(z,theta)* (((1j* omega)/P(0,theta)))
    # Fcoef[...,0,1]=(q*z**2*theta+omega - z*K*np.sqrt(P(z,theta)))
    # Fcoef[...,0,3]=ky *z*np.sqrt(P(z,theta))
    # Fcoef[...,1,0]=(q*z**2*theta+omega + K *z*np.sqrt(P(z,theta)))
    # Fcoef[...,1,1]=P(z,theta) *(1j*omega)/ P(0,theta)
    # Fcoef[...,1,2]=-  ky *z*np.sqrt(P(z,theta))
    # Fcoef[...,2,1]=ky *z*np.sqrt(P(z,theta))
    # Fcoef[...,2,2]= -(1j*omega*P(z,theta))/ P(0,theta)
    # Fcoef[...,2,3]=(q*z**2*theta+omega + K *z*np.sqrt(P(z,theta)))
    # Fcoef[...,3,0]=-ky *z*np.sqrt(P(z,theta))
    # Fcoef[...,3,2]=(q*z**2*theta+omega - K *z*np.sqrt(P(z,theta)))
    # Fcoef[...,3,3]=P(z,theta) *(1j*omega)/P(0,theta)

    #Usual Greeen function
    Zcoef[:,0,0,:]=0
    Fcoef[:,0,0,:]=0
    Xcoef[:,0,0,:]=0
    Zcoef[:,0,2,:]=0
    Fcoef[:,0,2,:]=0
    Xcoef[:, 0, 2, :] = 0
    Fcoef[:, 0, 0, 0] = 1
    Fcoef[:, 0, 2, 2] = 1

    Zcoef[:,N-1,...]=0
    Fcoef[:,N-1,...]=0
    Xcoef[:,N-1,...]=0
    
    Zcoef[:, N - 1, 0, 0]=P_0**2*(Fields[:,N-1, 1])**(0.5)*Fields[:,N-1, 3]
    Zcoef[:, N - 1, 0, 1] = -1j*P_0 ** 2 * (Fields[:, N - 1, 1]) ** (0.5) * Fields[:, N - 1, 3]
    Xcoef[:, N - 1, 0, 0]=2*(P_0*Fields[:,N-1, 3])**(3/2)
    Xcoef[:, N - 1, 0, 1] = 1j*2 * (P_0 * Fields[:, N - 1, 3]) ** (3 / 2)
    Fcoef[:,N-1,0,1]=-4*omega*(Fields[:,N-1, 1])**(0.5)*Fields[:,N-1, 3]*DP_0+2*omega*P_0*(Fields[:,N-1, 1])**(0.5)*(-DFields[:,N-1, 0, 3]+DFields[:,N-1, 3, 3])

    Zcoef[:, N - 1, 2, 2]=P_0**2*(Fields[:,N-1, 1])**(0.5)*Fields[:,N-1, 3]
    Zcoef[:, N - 1, 2, 3] = -1j*P_0 ** 2 * (Fields[:, N - 1, 1]) ** (0.5) * Fields[:, N - 1, 3]
    Xcoef[:, N - 1, 2, 2]=-2*(P_0*Fields[:,N-1, 3])**(3/2)
    Xcoef[:, N - 1, 2, 3] = -1j*2 * (P_0 * Fields[:, N - 1, 3]) ** (3 / 2)
    Fcoef[:,N-1,2,3]=-4*omega*(Fields[:,N-1, 1])**(0.5)*Fields[:,N-1, 3]*DP_0+2*omega*P_0*(Fields[:,N-1, 1])**(0.5)*(-DFields[:,N-1, 0, 3]+DFields[:,N-1, 3, 3])


    Fcoef[:, N-1, 1, 0] = -1j
    Fcoef[:, N-1, 1, 1] = 1
    Fcoef[:, N-1, 3, 2] = -1j
    Fcoef[:, N-1, 3, 3] = 1

    Xcoef=Xcoef.reshape(N**2,4,4)
    Zcoef=Zcoef.reshape(N**2,4,4)
    Fcoef=Fcoef.reshape(N**2,4,4)
    for i in range(4):
        for j in range (4):
            Jac[:,i,:,j]=np.multiply(Xcoef[:,i,j],XD.transpose()).transpose()+np.multiply(Zcoef[:,i,j],ZD.transpose()).transpose()+np.diag(Fcoef[:,i,j])
            
    return Jac.reshape(N**2*4,N**2*4)

