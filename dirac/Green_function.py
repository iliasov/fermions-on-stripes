import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
#import AdS_equations_bound as AdS
import Jacobian_Newton as Jac
from Jacobian_Newton import Newton_Dirac, Newton_Dirac_normed
import time
from scipy.interpolate import interp1d
import sys

def sol_Dirac(N, name):
    file = open(name, "r")
    solution = file.read()
    # x="[1,2]"
    solution = solution.replace("\n", "")
    solution = solution.replace("[", "")
    solution = solution.replace("]", "")
    solution = [complex(s) for s in solution.split()]

    # solution=eval('['+solution+']')
    # print(1)
    # print(solution)
    sol = np.zeros(N * N * 4, dtype=complex)
    for i in range(N * N * 4):
        sol[i] = solution[i]

    sol = sol.reshape((N, N, 4))
    file.close()
    return sol

def solutionDirac_func(Fields, Spinor, N, K, ky, omega, k0, theta, m, q, epsilon, filename, normed=True, basevector=0, write=True):
    Spinor = Spinor.reshape(N * N * 4)
    time1=time.time()
    if normed:
        solution = Newton_Dirac_normed(Fields, Spinor, N, K, ky, omega, k0, theta, m, q, epsilon, basevector)
    else:
        solution = Newton_Dirac(Fields, Spinor, N, K, ky, omega, k0, theta, m, q, epsilon)
    if write:
        solution = solution.reshape(N * N * 4)
    # try:
    #     os.makedirs(filename)
    # except OSError as exception:
    #     if exception.errno != errno.EEXIST:
    #         raise

        file = open(filename, "w")
        for i in range(N * N * 4):
            file.write(str(solution[i]) + ' ')
        file.close()
    solution=solution.reshape((N,N,4))
    time2=time.time()
    print("Spinor was calculated for", (time2-time1)/60, "min")
    return solution

def define_Fields(N, name, AdS=False, test=False, delta_x=0):
    if not test:

        if not AdS:
            file=open(name, "r")
            solution=file.read()
            # x="[1,2]"
            solution=solution.replace("\n","")
            solution=solution.replace("[","")
            solution=solution.replace("]","")
            solution=[float(s) for s in solution.split()]

            # solution=eval('['+solution+']')
            # print(1)
            # print(solution)
            sol=np.zeros(N*N*8)
            for i in range(N*N*8):
                sol[i]=solution[i]

            sol=sol.reshape((N,N,8))
            file.close()
            return sol
        if AdS:
            setka = (N, N)
            Fields=np.zeros((N,N,7))
            Fields[..., 0] = np.ones(setka)  # Qtt, Q-probe metric
            Fields[..., 1] = np.ones(setka)  # Qxx
            Fields[..., 2] = np.ones(setka)  # Qyy
            Fields[..., 3] = np.ones(setka)  # Qzz
            Fields[..., 4] = np.zeros(setka)  # Qxz
            Fields[..., 5] = theta*np.ones(setka)  # psi
            Fields[..., 6] = np.zeros(setka)  # phi
            return Fields

    if test:
        periodic = np.zeros((N,N))
        periodic1 = np.zeros((N, N))
        for i in range(N):
            periodic[i,:] = 0.3 * np.cos(k0 * delta_x * i)
        for i in range(N):
            periodic1[i, :] = 0.2 * np.sin(k0 * delta_x * i)
        setka = (N, N)
        Fields = np.zeros((N, N, 7))
        Fields[..., 0] = np.ones(setka)+periodic  # Qtt, Q-probe metric
        Fields[..., 1] = np.ones(setka)+periodic  # Qxx
        Fields[..., 2] = np.ones(setka)+periodic  # Qyy
        Fields[..., 3] = np.ones(setka)+periodic  # Qzz
        Fields[..., 4] = np.zeros(setka)#+periodic1  # Qxz
        Fields[..., 5] = theta * np.ones(setka)+periodic  # psi
        Fields[..., 6] = np.zeros(setka)  # phi
        return Fields

def coord_transform(z):
    return 1-z**2

def to_new_lattice(old_lattice, new_lattice, func_values):

    aux_lattice = coord_transform(old_lattice)
    #print(aux_lattice)
    if min(new_lattice) < min(aux_lattice) or max(new_lattice) > max(aux_lattice):
        print (min(new_lattice), max(new_lattice), min(aux_lattice), max(aux_lattice), 'Beyond interpolation limit')
        sys.exit()

    interpolation = interp1d(aux_lattice, func_values, kind='cubic')
    result = interpolation(new_lattice)

    return result

def save_interpolate_Fields(Fields, N, name):

    Fields_int=np.zeros((N,N,7))
    z_1 = np.zeros(N)
    z_end = 1
    z_start = 0
    for i in range(N):  # grid z axis
        z_1[i] = 0.5 * (z_end + z_start) + 0.5 * (z_end - z_start) * np.cos(np.pi * i / (N-1))

    for j in range(7):
        for i in range(N):
            Fields_int[i,:,j] = to_new_lattice(z_1, z_1, Fields[i,:,j])

    Fields_int=Fields_int.reshape(N*N*7)
    file = open(name, "w")
    for i in range(N * N * 7):
        file.write(str(Fields_int[i]) + ' ')
    file.close()

    Fields_int = Fields_int.reshape((N,N,7))

    return Fields_int

def cos_rotate(kx,ky):
    #return np.sign(ky)*np.sqrt(np.sqrt(kx**2+ky**2)+kx)
    return np.sign(ky) * np.sqrt(np.sqrt(kx ** 2 + ky ** 2) + kx)

def sin_rotate(kx,ky):
    return np.sqrt(np.sqrt(kx ** 2 + ky ** 2) - kx)




def Green_func(Fields, N, m, q, k0, theta, A0, Neps, N1, N2, file_name, type="omega_kx", omega=0.5, ky=0, K=0, kys=False, Kxs=False):
    epsilon = 10 ** (-Neps)
    if type=="omega_kx" or type==-1:
        omegas=np.linspace(0,2.5, N1)
        Kxs=np.linspace(-2,2, N2)
        if N1==1:
            omegas=[10**(-5)]
        if N2==1:
            Kxs=[0.3]
        # GR_xs=np.zeros((N), dtype=complex) #Green function for one value of omega in x-coordinate
        #GR_ks=np.zeros((N,2), dtype=complex) #Green function for one value of omega in k-coordinate
        GR_xfull=np.zeros((N, N2, N1, 4), dtype=complex) #Greem function for many values of omega

        # GR_xs[:,0]=epsilon**(-2*m)*Spinor_sol[:,0,0]/Spinor_sol[:,0,1]
        # GR_xs[:,1]=epsilon**(-2*m)*Spinor_sol[:,0,2]/Spinor_sol[:,0,3]
        # GR_ks=np.fft.fft(GR_xs, axis=0) #Green function for one value of omega in k-coordinate

        # print(GR_xs[:,0])
        # print(GR_xs[:,1])

        time1=time.time()
        for k in range(N2):
            K=Kxs[k]
            for i in range(N1):
                Spinor = np.zeros((N, N, 4), dtype=complex)
                omega=omegas[i]
                print("k ", k, "i ", i, "K ", K, " omega ",omega)

                Spinor_sol = solutionDirac_func(Fields, Spinor, N, K, ky, omega, k0, theta, m, q, epsilon,
                                                "Spinor_N=" + str(N) + " A0=" + str(A0) + "_theta=" + str(theta)
                                                + "_m=" + str(m) + "_omega=" + str(round(omega, 2)) + "_K=" + str(
                                                    round(K, 2)) + "_ky=" + str(ky) + "_q=" + str(q) + "_Neps=" + str(
                                                    Neps) + ".txt", basevector=0, write=False)

                GR_xfull[:, k, i, 0] = Spinor_sol[:, 0, 1]
                GR_xfull[:, k, i, 1] = Spinor_sol[:, 0, 3]

                Spinor = np.zeros((N, N, 4), dtype=complex)
                Spinor_sol = solutionDirac_func(Fields, Spinor, N, K, ky, omega, k0, theta, m, q, epsilon,
                                                "Spinor_N=" + str(N) + " A0=" + str(A0) + "_theta=" + str(theta)
                                                + "_m=" + str(m) + "_omega=" + str(round(omega, 2)) + "_K=" + str(
                                                    round(K, 2)) + "_ky=" + str(ky) + "_q=" + str(q) + "_Neps=" + str(
                                                    Neps) + ".txt", basevector=1, write=False)

                GR_xfull[:, k, i, 2] = Spinor_sol[:, 0, 1]
                GR_xfull[:, k, i, 3] = Spinor_sol[:, 0, 3]


        time2=time.time()
        print("Green function lasted", (time2-time1)/60, "min")


        file = open(file_name+ str(N) +"_A0="+str(A0)+ "_theta=" + str(
                                                theta) + "_m="+str(m)+"_q="+str(q)+"_ky="+str(ky)+"_Neps="+str(Neps)+".txt", "w")
        GR_xfull=GR_xfull.reshape(N2*N1*N*4)
        file.write('-1 '+str(N)+' '+ str(N1)+' '+str(N2)+ ' '+str(theta)+' '+str(k0)+' '+str(ky)+' ')
        for i in range(N1):
            file.write(str(omegas[i]) + ' ')
        for i in range(N2):
            file.write(str(Kxs[i]) + ' ')
        for i in range(N2 * N1*N*4):
            file.write(str(GR_xfull[i]) + ' ')
        file.close()
        print (GR_xfull.shape)

    if type=="kx_ky" or type==-2:

        # kys = np.linspace(-3, 3, N1)
        # Kxs = np.linspace(-3, 3, N2)

        if N1 == 1:
            kys = [0.3]
        if N2 == 1:
            Kxs = [0.0]
       
        GR_xfull = np.zeros((N, N2, N1, 4), dtype=complex)  # Greem function for many values of omega


        time1 = time.time()
        for k in range(N2):
            K = Kxs[k]
            for i in range(N1):
                Spinor = np.zeros((N, N, 4), dtype=complex)
                ky = kys[i]
                print("k ", k, "i ", i, "K ", K, " ky ", ky)
                Spinor_sol = solutionDirac_func(Fields, Spinor, N, K, ky, omega, k0, theta, m, q, epsilon,
                                                "Spinor_N=" + str(N) + " A0=" + str(A0) + "_theta=" + str(theta)
                                                + "_m=" + str(m) + "_omega=" + str(round(omega, 2)) + "_K=" + str(
                                                    round(K, 2)) + "_ky="+str(ky)+"_q=" + str(q) + "_Neps=" + str(
                                                    Neps) + ".txt", basevector=0, write=False)

                GR_xfull[:, k, i, 0] = Spinor_sol[:, 0, 1]
                GR_xfull[:, k, i, 1] = Spinor_sol[:, 0, 3]

                Spinor = np.zeros((N, N, 4), dtype=complex)
                Spinor_sol = solutionDirac_func(Fields, Spinor, N, K, ky, omega, k0, theta, m, q, epsilon,
                                                "Spinor_N=" + str(N) + " A0=" + str(A0) + "_theta=" + str(theta)
                                                + "_m=" + str(m) + "_omega=" + str(round(omega, 2)) + "_K=" + str(
                                                    round(K, 2)) + "_ky="+str(ky)+"_q=" + str(q) + "_Neps=" + str(
                                                    Neps) + ".txt", basevector=1, write=False)

                GR_xfull[:, k, i, 2] = Spinor_sol[:, 0, 1]
                GR_xfull[:, k, i, 3] = Spinor_sol[:, 0, 3]



        time2 = time.time()
        print("Green function lasted", (time2 - time1) / 60, "min")

        file = open(file_name+ str(N) + " A0=" + str(A0) + "_theta=" + str(
            theta) + "_m=" + str(m) + "_q=" + str(q) + " omega="+str(round(omega, 2))+"_Neps=" + str(Neps) +"_k0=" +str(k0)+".txt", "w")
        GR_xfull = GR_xfull.reshape(N2 * N1 * N * 4)
        file.write(
            '-2 ' + str(N) + ' ' + str(N1) + ' ' + str(N2) + ' ' + str(theta) + ' ' + str(k0) + ' ' + str(omega) + ' ')
        for i in range(N1):
            file.write(str(kys[i]) + ' ')
        for i in range(N2):
            file.write(str(Kxs[i]) + ' ')
        for i in range(N2 * N1 * N * 4):
            file.write(str(GR_xfull[i]) + ' ')
        file.close()
        print(GR_xfull.shape)




N=20
N1 = 200
N2 = 200
L=1
k0 = 0.7
theta = 2.3
A0=7.07

omega=10**(-5)
K=0.0
ky=0
m=0
q=1.5
Neps=15
epsilon=10**(-Neps)
print("epsilon=", epsilon)


k0=0.6
q=1.5
N1 = 500
N2 = 500
kys = np.linspace(-2.1, 2.1, N1)
Kxs = np.linspace(-2.4, 2.4, N2)

Spinor=np.zeros((N,N,4), dtype=complex)
Fields = define_Fields(N, "Stripes_QS_trans_N=20_A0=5.0_k0=0.6_theta=2.3_tau=1_M(imag)=1.41.txt", test=False, delta_x=(2 * np.pi / (k0*N)), AdS=False)
Fields_int = save_interpolate_Fields(Fields, N, "Qlat_Stripes_Int_tau=1_N=20_A0=" + str(A0) + "_k0=" + str(k0) + "_theta=" + str(theta) + ".txt")
Green_func(Fields_int, N, m, q, k0, theta, A0, Neps, N1, N2, "GR_Stripes_tau=1_Qlat_N=", type=-2, omega=omega, ky=ky, K=K,kys=kys, Kxs=Kxs)



