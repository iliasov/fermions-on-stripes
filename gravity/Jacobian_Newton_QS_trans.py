import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
# import AdS_equations_bound as AdS
from scipy.optimize import root
#from AdS_equations_bound import AdS_stripes, DeTurktensor_test, DeTurk_test
#from Dirac_eq import Dirac_solve, background_solve, D_num_func, Diff
#from Dirac_norm import Dirac_normed
from AdS_equations_norm_QS_trans import AdS_stripes_norm, xi_norm

def Jacobian(N, A0, k0, theta, Fields, L, epsilon):

    eps=10**(-10)
    Jac=np.zeros((N**2*8,N**2*8))
    Result_0=AdS_stripes(Fields, N, A0, k0, theta, L, epsilon)
    for i in range(N**2*8):
        Fields[i]+=eps
        Jac[:,i]=(AdS_stripes(Fields, N,A0, k0, theta, L, epsilon)-Result_0)/eps
        Fields[i] -= eps
        # for j in range(N**2*7):
        #     if Jac[j,i]<eps:
        #         Jac[j,i]=0


    return Jac

def Jacobian_normed(N, A0, k0, theta, Fields, L,k1,M,tau):

    eps = 10 ** (-10)
    Jac = np.zeros((N ** 2 * 8, N ** 2 * 8))
    Result_0 = AdS_stripes_norm(Fields, N, A0, k0, theta, L,k1,M,tau)
    for i in range(N ** 2 * 8):
        Fields[i] += eps
        Jac[:, i] = (AdS_stripes_norm(Fields, N, A0, k0, theta, L,k1,M,tau) - Result_0) / eps
        Fields[i] -= eps
        # for j in range(N**2*7):
        #     if Jac[j,i]<eps:
        #         Jac[j,i]=0

    return Jac


def Newton(Fields_0, N, A0, k0, theta, L, epsilon):

    x=Fields_0
    for i in range(1000):

        y=AdS_stripes(x,N, A0, k0, theta, L, epsilon)
        tol=np.max(np.abs(y))
        print("tol",tol, "xi_norm", np.max(np.abs(xi_norm(x, theta, L, N, k0,k1,M,tau))))
        if tol>10**(4):
            print("scheme is not converging")
            return False
        if tol<10**(-8):
            print("solution has been found")
            break
        Jac=Jacobian(N, A0, k0, theta, x, L, epsilon)
        delta_x=np.linalg.solve(Jac, -y)
        # print('Fields', delta_x.reshape((N,N,7)))
        # print('Result', y.reshape((N,N,7)))
        x+=delta_x

    return x

def Newton_normed(Fields_0, N, A0, k0, theta, L,k1,M,tau):

    x=Fields_0
    for i in range(1000):

        y=AdS_stripes_norm(x,N, A0, k0, theta, L,k1,M,tau)
        tol=np.max(np.abs(y))
        print("tol",tol, "xi_norm", np.max(np.abs(xi_norm(x, theta, L, N, k0,k1,M,tau))))
        if tol>10**(4):
            print("scheme is not converging")
            return np.zeros(N*N*8)
        if tol<10**(-8):
            print("solution has been found")
            break
        Jac=Jacobian_normed(N, A0, k0, theta, x, L,k1,M,tau)
        delta_x=np.linalg.solve(Jac, -y)
        # print('Fields', delta_x.reshape((N,N,7)))
        # print('Result', y.reshape((N,N,7)))
        x+=delta_x

    return x
