import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from scipy.optimize import root
#from AdS_equations_bound import AdS_stripes
from AdS_equations_norm_QS_trans import AdS_stripes_norm, R_tr
from AdS_equations_norm_QS_trans import xi_norm
from Jacobian_Newton_QS_trans import Newton, Newton_normed
#import Equations_test as eq_t
#import AdS_equations_bound as AdS
import os
import errno
import glob
import re
# import RNsolution_test as RNtest
import time

def define_Fields(N, name):

    file=open(name, "r")
    solution=file.read()
    # x="[1,2]"
    solution=solution.replace("\n","")
    solution=solution.replace("[","")
    solution=solution.replace("]","")
    solution=[float(s) for s in solution.split()]

    # solution=eval('['+solution+']')
    # print(1)
    # print(solution)
    sol=np.zeros(N*N*8)
    for i in range(N*N*8):
        sol[i]=solution[i]

    sol=sol.reshape((N,N,8))
    file.close()
    return sol

def solution_func(Fields, N, A0, k0, theta, L, epsilon, k1,M,tau,filename, normed=True):
    Fields = Fields.reshape(N * N * 8)
    if normed:
        solution = Newton_normed(Fields, N, A0, k0, theta, L,k1,M,tau)
        if solution.any()==0:
            #print(solution)
            print("no solution")
            return False
    else:
        solution = Newton(Fields, N, A0, k0, theta, L, epsilon,k1,M,tau)
    # print("DeTurk_norm", np.max(np.abs(AdS.DeTurk_test(Fields, N, k0, theta, L))))
    solution = solution.reshape(N * N * 8)
    # try:
    #     os.makedirs(filename)
    # except OSError as exception:
    #     if exception.errno != errno.EEXIST:
    #         raise
    file = open(filename, "w")
    for i in range(N * N * 8):
        file.write(str(solution[i]) + ' ')
    file.close()

def R_in_dir():
    list=glob.glob("Stripes_Qlat_N*.txt")
    for title in list:
        rr = re.findall("[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?", title)
        print(title+" is processed")
        N=int(rr[0])
        A0=float(rr[1])
        k0=float(rr[2])
        theta=float(rr[3])
        Fields = define_Fields(N, title)

        R=R_tr(Fields, theta, L, N, k0)
        R=R.reshape(N*N)
        file = open("R_tr_"+title, "w")
        for i in range(N * N ):
            file.write(str(R[i]) + ' ')
        file.close()

N=20 #number of vertices in grid
theta = 2.3 #chemical potential
L=1 #AdS radius
k1=2.1 
k0 = k1 #quasimomentum in x-direction
Tx = 2 * np.pi / k0  # period of a lattice
A0=5.0 #amplitude of potential
M=1j*np.sqrt(2) #scalar field mass
tau=1.0 #parameter defining transition between Q-lattices and Stripes
epsilon=0

grid=((N,N))
Fields=np.zeros((N,N,8))

Fields[...,0] = np.ones(grid)  #Qtt, Q-probe metric
Fields[...,1] = np.ones(grid)    #Qxx
Fields[...,2] = np.ones(grid)    #Qyy
Fields[...,3] = np.ones(grid)   #Qzz
Fields[...,4] = np.zeros(grid)    #Qxz
Fields[...,5] = theta*np.ones(grid)    #psi
Fields[...,6] = A0*np.ones(grid)   #phi1Re
Fields[...,7] = tau*A0*np.ones(grid)   #phi1im
#Fields[...,7] = np.zeros(grid)   #phi1im
#Fields = define_Fields(N, "Stripes_QS_trans_N=20_A0="+str(A0)+"_k0="+str(k0)+"_theta=2.3_tau="+str(tau)+"_M(imag)=1.41.txt")

time1=time.time()

solution_func(Fields, N, A0, k0, theta, L, epsilon, k1,M,tau,"Stripes_QS_trans_N="+str(N)+"_A0="+str(A0)+"_k0="+str(k0)+
                   "_theta="+str(theta)+"_tau="+str(tau)+"_M(imag)="+str(round(np.imag(M),2))+".txt")

time2=time.time()
print("calculations for N=", N," lasted ", (time2-time1)/60, "min")



